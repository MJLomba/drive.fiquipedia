1
00:00:00,179 --> 00:00:03,179
En este vídeo vamos a hablar sobre
los fundamentos de los moles

2
00:00:03,179 --> 00:00:05,920
vamos a aprender sobre lo que son
y vamos a hablar sobre alguno de 

3
00:00:05,920 --> 00:00:07,201
esos importantes números y términos

4
00:00:08,029 --> 00:00:11,620
Vamos a empezar desde cero
así que no importa 

5
00:00:11,620 --> 00:00:13,241
cuanto sabes o cuanto no sabes sobre moles

6
00:00:13,799 --> 00:00:17,060
este vídeo será un buen lugar para empezar

7
00:00:17,006 --> 00:00:20,065
Qué es un mol? No estoy hablando aquí sobre la 
pequeña criatura peluda que vive

8
00:00:20,065 --> 00:00:21,147
bajo tierra (mole en inglés también significa topo)

9
00:00:21,047 --> 00:00:24,121
estoy hablando sobre los moles que
son super importantes en química

10
00:00:25,021 --> 00:00:28,033
Qué es un mol? un mol es un nombre 

11
00:00:28,033 --> 00:00:31,065
para un número específico de cosas, ok?

12
00:00:31,065 --> 00:00:34,078
un mol es algo parecido a una docena,
sí una docena

13
00:00:34,078 --> 00:00:37,155
es el nombre para 12 cosas, 
hay 12 cosas en una docena 

14
00:00:38,055 --> 00:00:41,069
ahora, un mol es como eso pero algo más grande
que una docena

15
00:00:41,069 --> 00:00:44,071
12 cosas en una docena, en un mol

16
00:00:44,071 --> 00:00:47,190
hay 602

17
00:00:47,829 --> 00:00:50,920
mil trillones de cosas (en inglés hexillion son 1000 trillones)
, esto es 602

18
00:00:51,739 --> 00:00:54,764
seguido por 21 ceros

19
00:00:54,989 --> 00:00:58,550
602 hexillones de cosas, de modo que un mol

20
00:00:58,055 --> 00:01:02,334
como una docena, es un nombre para un cierto
número de cosas. Hay 12 cosas en una docena

21
00:01:02,829 --> 00:01:06,430
y hay 602 mil trillones

22
00:01:06,043 --> 00:01:10,092
de cosas en un mol
Esto es un número gigantesco

23
00:01:10,479 --> 00:01:13,760
Hay una cosa que confunde a la gente sobre los moles

24
00:01:13,076 --> 00:01:16,089
olvidan que un mol es un nombre para 602 

25
00:01:16,089 --> 00:01:19,091
mil trillones de cosas y piensan que un mol

26
00:01:20,009 --> 00:01:24,103
es una abreviatura de la palabra molécula
de modo que mucha gente piensa esto, ellos ven mol

27
00:01:25,103 --> 00:01:26,090
y piensan que quieres decir molécula, no es así!

28
00:01:26,090 --> 00:01:31,658
no quiero decir molécula
quiero decir un grupo de 602 mil trillones de cosas, 

29
00:01:32,549 --> 00:01:32,960
no una molécula

30
00:01:32,096 --> 00:01:35,105
De modo que hay 602 mil trillones de cosas en un mol

31
00:01:36,086 --> 00:01:39,165
y al igual que con una docena, podemos tener un mol

32
00:01:39,939 --> 00:01:43,021
de cualquier cosa, ok? 
podemos tener una docena de donuts

33
00:01:43,759 --> 00:01:47,310
que serían 12 donuts
o podemos tener

34
00:01:47,031 --> 00:01:52,042
un mol de donuts que serían 602 mil trillones

35
00:01:52,042 --> 00:01:55,051
de donuts
o podemos usar gominolas

36
00:01:56,032 --> 00:01:59,095
una docena de gominolas serían 12 gominolas

37
00:01:59,095 --> 00:02:02,874
y un mol de gominolas serían 

38
00:02:03,729 --> 00:02:06,740
602 mil trillones de gominolas

39
00:02:06,074 --> 00:02:09,953
A veces la gente se asusta de lo monstruosos que son los moles
recuerda que son

40
00:02:10,619 --> 00:02:12,040
un lote como una docena

41
00:02:12,004 --> 00:02:16,005
12 cosas en una docena

42
00:02:16,005 --> 00:02:19,083
602 mil trillones de cosas en un mol
Puedes tener más moles, un mol 

43
00:02:19,083 --> 00:02:23,109
de gominolas, un mol de coches, un mol de clips
un mol de pelotas

44
00:02:24,009 --> 00:02:27,068
siempre que sean 602 mil trillones de cosas

45
00:02:27,068 --> 00:02:31,657
tendrás un mol
Vamos a hablar un poco más sobre este número

46
00:02:32,269 --> 00:02:35,920
602 mil trillones.
Este número es

47
00:02:35,092 --> 00:02:38,161
a menudo es citado como el número de Avogadro

48
00:02:39,061 --> 00:02:43,092
en honor del científico italiano que lo descubrió


49
00:02:43,092 --> 00:02:46,100
Ahora, mira esto, 602 mil trillones

50
00:02:47,072 --> 00:02:52,104
es un número gigantesco
602 con 21 ceros detrás

51
00:02:53,004 --> 00:02:56,075
Piensa en lo difícil que es manejar este número, ok?

52
00:02:56,075 --> 00:02:59,158
si haces matemáticas a mano tendrás que escribir y hacer

53
00:03:00,058 --> 00:03:05,075
tus muultiplicaciones y divisiones con 21 ceros
incluso si usas una calculadora

54
00:03:05,075 --> 00:03:09,084
tienes que escribir todos estos ceros y asegurarte
de que tienes en número correcto y cuando tengas

55
00:03:09,084 --> 00:03:10,109
 el resultado

56
00:03:10,009 --> 00:03:13,268
tendrás que contar cuantos ceros tiene la respuesta

57
00:03:13,349 --> 00:03:18,374
es muy tedioso, de modo que en lugar de escribir 602

58
00:03:18,599 --> 00:03:22,590
mil trillones con todos los ceros cada vez
que se habla de moles

59
00:03:22,059 --> 00:03:25,087
se tiende a abreviar este número 

60
00:03:25,087 --> 00:03:29,103
como puede que sepas que se abrevian 
números en matemáticas y en ciencia

61
00:03:30,003 --> 00:03:34,292
donde usamos una técnica de notación científica
y aquí es como vamos a 

62
00:03:34,319 --> 00:03:35,750
abreviar ese número

63
00:03:35,075 --> 00:03:39,133
usando notiación científica, ok
Si tomo ese número 602 mil trillones encuentro que

64
00:03:40,033 --> 00:03:41,552
donde está la coma sería 

65
00:03:41,849 --> 00:03:46,690
aquí y ahora muevo la coma hasta que tengo

66
00:03:46,069 --> 00:03:46,134
un único dígito

67
00:03:47,034 --> 00:03:50,047
en la izquierda, ok, y cuento el número

68
00:03:50,047 --> 00:03:54,061
de modo que hay 1 2 3 4 5 6

69
00:03:54,061 --> 00:03:57,078
7 8 9 10 11 12

70
00:03:57,078 --> 00:04:00,597
13 14 15 16 17 18

71
00:04:01,299 --> 00:04:05,970
19 20 21 22 23

72
00:04:05,097 --> 00:04:08,106
y ahora este es mi nueva posición porque
hay solamente un dígito

73
00:04:09,087 --> 00:04:12,145
a la izquierda. He desplazado la coma

74
00:04:13,045 --> 00:04:16,604
23 posiciones a la izquierda

75
00:04:17,009 --> 00:04:20,120
y eso significa que en notación científica

76
00:04:20,012 --> 00:04:23,311
puedo reescribir este número como 

77
00:04:23,419 --> 00:04:27,220
6,02 por 10 elevado 

78
00:04:27,022 --> 00:04:31,050
a 23. El 23 viene del número de posiciones

79
00:04:31,005 --> 00:04:36,014
que desplacé la coma a la izquierda
De modo que 6,02 por 10 elevado a 23

80
00:04:36,509 --> 00:04:39,910
es como abreviamos normalmente este número gigantesco

81
00:04:39,091 --> 00:04:44,132
602 mil trillones cuando queremos escribirlo
y usarlo en problemas matemáticos

82
00:04:45,032 --> 00:04:48,110
esta es una manera adecuada de escribir 
este número giganteso en notación científica

83
00:04:49,001 --> 00:04:51,059
pero también es una manera terrible de escribirlo

84
00:04:51,068 --> 00:04:54,070
porque este número 6,02 por 10 elevado a 23

85
00:04:54,088 --> 00:04:57,184
tiene un aspecto terrible, ok
tiene este 10 con el exponente

86
00:04:58,084 --> 00:05:01,088
6.02 por 10 elevado a 23. 
La gente ve este número y es como

87
00:05:02,024 --> 00:05:06,453
oh dios mío, qué es lo que voy a hacer con esto!?

88
00:05:06,669 --> 00:05:11,910
tranquilo, está bien, 6,02 por 10 elevado a 23

89
00:05:11,091 --> 00:05:14,570
puede tener un aspecto terrible, pero
simplemente recuerda

90
00:05:15,389 --> 00:05:19,550
que es una abreviación de 602 mil trillones

91
00:05:19,055 --> 00:05:22,294


92
00:05:22,789 --> 00:05:26,560
de este número tan grande

93
00:05:26,056 --> 00:05:30,125
Cada vez que veas 6,02 por 10 elevado a 23 recuerda

94
00:05:31,025 --> 00:05:35,111
que es una abreviación de 602 mil trillones

95
00:05:36,011 --> 00:05:39,037
ok, ya hemos aprendido qué es un mol

96
00:05:39,037 --> 00:05:43,037
cuantas cosas hay en un mol 
y hemos aprendido como abreviar este número gigantesco

97
00:05:43,037 --> 00:05:45,091
602 hexillones

98
00:05:45,091 --> 00:05:50,124
lo último que quiero hacer es darte una idea
de cómo de gigantesco es este número

99
00:05:51,024 --> 00:05:55,028
602 mil trillones, cómo de gigantesco es

100
00:05:55,028 --> 00:05:58,032
ok, vamos a pensar sobre este mol

101
00:05:58,068 --> 00:06:02,103
de gominolas del que hablamos antes

102
00:06:03,003 --> 00:06:08,050
el cual sabes que son 6,02 por 10 elevado a 23

103
00:06:08,005 --> 00:06:09,052
gominolas, son simplemente

104
00:06:09,097 --> 00:06:12,180
tres maneras de escribir el mismo número, ok?

105
00:06:13,008 --> 00:06:16,877
si tienes 602 

106
00:06:17,669 --> 00:06:20,728
mil trillones de gominolas

107
00:06:21,259 --> 00:06:25,270
sería tan grande 

108
00:06:25,027 --> 00:06:28,071
como todo el planeta Tierra

109
00:06:28,071 --> 00:06:32,370
Un mol de gominolas sería tan grande como todo el planeta Tierra

110
00:06:33,009 --> 00:06:36,065
es una locura pensar cuantas gominolas

111
00:06:36,569 --> 00:06:39,850
piensa cuantas gominolas habría si tu casa

112
00:06:39,085 --> 00:06:42,160
se llenase con gominolas, ok, eso sería enorme

113
00:06:43,006 --> 00:06:47,024
piensa cuantas gominolas habría si cada edificio de tu ciudad

114
00:06:47,078 --> 00:06:50,367
se llena con gominolas, ok

115
00:06:51,069 --> 00:06:54,138
Eso sería un montón
Ahora imagina 

116
00:06:54,759 --> 00:06:58,020
cuantas gominolas tienes si la Tierra entera 

117
00:06:58,002 --> 00:07:01,711
estuviera hecha de nada más que de gominolas

118
00:07:01,729 --> 00:07:04,910
y eso es cómo de grande un mol

119
00:07:04,091 --> 00:07:09,440
de algo sería
También podemos tener un mol de donuts

120
00:07:10,259 --> 00:07:13,306
que serían 602

121
00:07:13,729 --> 00:07:17,650
mil millones de donuts, 
6,02 por 10 elevado a 23 donuts

122
00:07:17,065 --> 00:07:20,454
si tenemos todos esos donuts

123
00:07:21,039 --> 00:07:25,910
y los apilamos uno encima de otro así

124
00:07:25,091 --> 00:07:28,186
y tenemos 602 mil trillones de donuts y realmente hacemos esto
y continuamos haciendo la pila 

125
00:07:29,086 --> 00:07:32,111
esta pila llegaría desde la Tierra

126
00:07:33,011 --> 00:07:37,016
hasta el Sol, y vuelta

127
00:07:37,016 --> 00:07:40,037
200 mil millones de veces (en inglés billion son mil millones)

128
00:07:40,037 --> 00:07:43,146
la pila de donuts llegaría

129
00:07:43,479 --> 00:07:47,150
desde la Tierra hasta el Sol, de un lado a otro

130
00:07:47,015 --> 00:07:51,064
200 mil millones de veces 
de nuevo piensa en cuantos donuts

131
00:07:51,199 --> 00:07:54,204
tendríamos si los ponemos así

132
00:07:54,699 --> 00:07:57,771
desde tu casa hasta el colegio, ok
eso sería un montón de donuts

133
00:07:58,419 --> 00:08:01,500
ahora imagina ir de la Tierra al Sol y vuelta

134
00:08:01,005 --> 00:08:05,054
200 mil millones de veces 
eso es cuantos donuts

135
00:08:05,099 --> 00:08:09,168
son 602 mil trillones de donuts

136
00:08:10,068 --> 00:08:13,102
pero ahora aquí hay una cosa que es realmente interesante

137
00:08:14,002 --> 00:08:17,461
estamos hablando aquí sobre gominolas y donuts

138
00:08:17,479 --> 00:08:21,470
pero muchas veces en química estamos 
hablando de cosas mucho más pequeñas

139
00:08:21,047 --> 00:08:24,093
estamos hablando de átomos

140
00:08:24,093 --> 00:08:27,184
ahora, un mol de gominolas sería tan grande

141
00:08:28,084 --> 00:08:31,090
como el planeta Tierra, pero un mol

142
00:08:32,044 --> 00:08:35,123
de átomos es mucho más pequeño

143
00:08:35,519 --> 00:08:38,523
aquí en este recipiente tengo aproximadamente 

144
00:08:38,919 --> 00:08:42,060
un mol de átomos de azufre, este polvo amarillo

145
00:08:42,006 --> 00:08:46,725
y estas esquirlas amarillas están
hechas totalmente de átomos de azufre

146
00:08:46,779 --> 00:08:47,510
y hay unos

147
00:08:47,051 --> 00:08:50,086
602 mil trillones de átomos de azufre

148
00:08:50,086 --> 00:08:54,088
en este platillo
Piensa en ello, ok

149
00:08:55,006 --> 00:08:58,013
tenemos 602

150
00:08:58,013 --> 00:09:02,079
mil trillones de átomos de azufre justo aquí ahora
602 mil trillones

151
00:09:02,079 --> 00:09:05,104
gominolas tendrían el tamaño del planeta Tierra

152
00:09:06,004 --> 00:09:11,085
602 mil trillones de átomos de azufre caben en 
este platillo 

153
00:09:11,085 --> 00:09:15,944
esto nos muestra como de increíblemente diminuto

154
00:09:16,709 --> 00:09:20,709
es un átomo de azufre,
cómo de diminutos son todos los átomos

155
00:09:20,709 --> 00:09:24,500
comparados con una gominola
realmente un mol de esto 

156
00:09:24,005 --> 00:09:27,098
tiene el tamaño de la Tierra
pero un mol de estos átomos de azufre

157
00:09:28,043 --> 00:09:33,322
cabe en este plato
De modo que cuando manejamos cosas tan diminutas 
como los átomos

158
00:09:33,709 --> 00:09:37,797
un mol realmente no ocupa tanto espacio

159
00:09:38,589 --> 00:09:39,070
porque las cosas con las que estamos tratando

160
00:09:39,007 --> 00:09:43,056
son tan increíblemente pequeñas que un número gigante de ellas

161
00:09:43,056 --> 00:09:46,143
no es tan grande
Vamos a repasar sobre lo que hemos hablado

162
00:09:47,043 --> 00:09:52,082
vamos a volver un poco sobre las ideas clave
Un mol es como una docena

163
00:09:52,082 --> 00:09:56,781
salvo que hay 12 cosas en una docena

164
00:09:57,519 --> 00:10:00,950
y 602 mil trillones en un mol
Podemos escribir este número

165
00:10:00,095 --> 00:10:03,107
de varias formas
lo podemos hacer como 602 con 

166
00:10:04,007 --> 00:10:07,035
21 ceros detrás
podemos llamarlo 602 mil trillones

167
00:10:07,035 --> 00:10:11,039
o más a menudo, podemos abreviar este 
número en notación científica

168
00:10:11,039 --> 00:10:14,097
6,02 por 10 elevado a 23

169
00:10:14,097 --> 00:10:18,146
este número de aquí 602 mil trillones 
a menudo se cita como

170
00:10:19,019 --> 00:10:25,044
el número de Avogadro, y finalmente, 
al final de esta lección vimos como 602

171
00:10:25,269 --> 00:10:30,350
mil trillones es un número gigante
602 mil trillones de gominolas tendrían el tamaño

172
00:10:30,035 --> 00:10:34,524
del planeta Tierra, pero los átomos son tan diminutos

173
00:10:34,839 --> 00:10:38,100
que un mol de átomos realmente

174
00:10:38,001 --> 00:10:43,030
no ocupa mucho espacio
602 mil trillones de átomos de azufre, por ejemplo

175
00:10:43,039 --> 00:10:46,093
son de un tamaño bastante manejable


176
00:10:46,093 --> 00:10:48,100
Y esto es una introducción a los moles
